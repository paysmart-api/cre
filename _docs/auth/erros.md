---
title: Códigos de Retorno
name: erros
tags:
    - auth
    - erros
description: Lista de possíveis códigos retornados pelo Autorizador.
type: auth
---
{% include erros_header.html %}

<table>
    <thead>
        <tr>
            <th>Código</th>
            <th>Mensagem</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>0</td>
            <td>Comando processado com sucesso</td>
        </tr>
        <tr>
            <td>1</td>
            <td>Parâmetro inválido – tamanho dos dados difere do esperado</td>
        </tr>
        <tr>
            <td>2</td>
            <td>Parâmetro obrigatório faltando na construção do pacote</td>
        </tr>
        <tr>
            <td>3</td>
            <td>Formato do comando inválido</td>
        </tr>
        <tr>
            <td>4</td>
            <td>Código de comando inválido</td>
        </tr>
        <tr>
            <td>5</td>
            <td>Parâmetro inválido - caracteres inválidos</td>
        </tr>
        <tr>
            <td>6</td>
            <td>Comando inválido</td>
        </tr>
        <tr>
            <td>7</td>
            <td>Número de parâmetros inválidos</td>
        </tr>
        <tr>
            <td>50</td>
            <td>Criptograma inválido</td>
        </tr>
        <tr>
            <td>51</td>
            <td>Versão de criptograma não suportada</td>
        </tr>
        <tr>
            <td>53</td>
            <td>Chave do Emissor não encontrada</td>
        </tr>
        <tr>
            <td>54</td>
            <td>AID não suportado</td>
        </tr>
        <tr>
            <td>55</td>
            <td>Falha na comunicação com o HSM</td>
        </tr>
        <tr>
            <td>56</td>
            <td>Falha na operação do HSM</td>
        </tr>
        <tr>
            <td>57</td>
            <td>Tempo de espera esgotado</td>
        </tr>
        <tr>
            <td>58</td>
            <td>Erro ao importar uma chave pré-existente</td>
        </tr>
        <tr>
            <td>59</td>
            <td>Erro ao tentar realizar operação administrativa antes de validar criptograma</td>
        </tr>
        <tr>
            <td>60</td>
            <td>Conteúdo inválido dos dados TLV</td>
        </tr>
        <tr>
            <td>61</td>
            <td>Faltando dados obrigatórios</td>
        </tr>
        <tr>
            <td>70</td>
            <td>Inconsistência de dados</td>
        </tr>
        <tr>
            <td>93</td>
            <td>Falha na validação offline</td>
        </tr>
        <tr>
            <td>94</td>
            <td>Suspeita de fraude</td>
        </tr>
        <tr>
            <td>95</td>
            <td>Suspeita de fraude</td>
        </tr>
        <tr>
            <td>96</td>
            <td>Suspeita de fraude</td>
        </tr>
        <tr>
            <td>97</td>
            <td>Suspeita de fraude</td>
        </tr>
        <tr>
            <td>98</td>
            <td>Erro Interno</td>
        </tr>
        <tr>
            <td>99</td>
            <td>Erro Interno</td>
        </tr>
    </tbody>
</table>