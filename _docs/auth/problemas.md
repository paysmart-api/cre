---
title: "Checklist para Resolução de Problemas"
name: problemas
description: "Erros de criptograma inválido (50) muitas vezes são difíceis de mapear porque podem ser causados por muitos motivos. <br>Essa seção contém um checklist de parâmetros a serem verificados antes de suspeitar de outros problemas mais graves como tentativas de fraude."
type: auth
---
{% include erros_header.html %}

**1)** É um cartão de testes ou de produção? Cartões de testes são personalizados com chaves de teste e não podem ser utilizados em ambiente de produção. Cartões de produção, por sua vez, são personalizados com chaves de produção e não podem ser utilizados em ambiente de testes.

**2)** Todos os dados obrigatórios estão sendo enviados para o autorizador? A documentação sobre o endpoint <a href="./authorize">/auth/authorize</a> descreve todos os dados obrigatórios e como eles podem ser enviados diretamente na área EMV.

**3)** A aplicação selecionada (por exemplo, débito ou crédito) pelo terminal durante a transação é a mesma que está sendo informada na autorização através do elemento 84 (AID)? O AID está sendo enviado diretamente pelo adquirente ou passado pela aplicação chamadora?

**4)** O PAN (*Primary Account Number*) está sendo passado diretamente no bit 55 ou foi passado por parâmetro? No caso de estar sendo recuperado do bit 2, está sendo adicionado no parâmetro corretamente? O PAN Foi decriptado de algum outro sistema? O PAN faz sentido em relação aos últimos dígitos do cartão?

**5)** A data da transação (*Transaction Date*) que está sendo enviada faz sentido? É a mesma data do terminal? Tem o mesmo formato EMV (AAMMDD) utilizado pelo cartão? Foi recuperada do bit 13 ou está nos dados EMV?

**6)** O Valor da Transação (*Amount Authorized*) está sendo recuperado do bit 4 e sendo enviado por parâmetro ou está na área EMV? O valor faz sentido? É o mesmo valor da transação no terminal?

**7)** O Valor de Cashback (*Amount Other*) está sendo recuperado do bit 54 e sendo enviado por parâmetro ou está na área EMV? O valor faz sentido? (Tipicamente é zero no Brasil). É o mesmo valor da transação no terminal? 

**8)** O *PAN Sequence Number (PANSeq)* está sendo recuperado do bit 23 e sendo enviado por parâmetro ou está na área EMV? Verificar se não está sendo omitido no pacote de autorização (e assumindo o valor default 0) e se foi personalizado no cartão. Para cartões Mastercard Múltiplos, por exemplo, o PAN Sequence normalmente é 01 para Crédito e 02 para Débito.


**9)** O Código da Moeda da transação (*Transaction Currency Code*) é o mesmo utilizado pelo terminal? (Dica: 0986=Real)


**10)** O Código do País do Terminal (*Terminal Country Code*) é o mesmo do Terminal? <br>(Dica: 0076=Brasil) 
