---
title: Códigos de Erro da API
tags:
    - cre api
    - erros
description: "Lista de possíveis códigos de erro da CRE API"
name: "erros_api"
---
{% include service_header.html %}

<table>
    <thead>
        <tr>
            <th>Código</th>
            <th>Mensagem</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>40003</td>
            <td>O cabeçalho X-API-Key não está definido</td>
        </tr>
        <tr>
            <td>40103</td>
            <td>Chave de API inválida</td>
        </tr>
        <tr>
            <td>40303</td>
            <td>Chave de API não pertence ao cliente</td>
        </tr>
        <tr>
            <td>40403</td>
            <td>Nenhum comando correspondente encontrado</td>
        </tr>
    </tbody>
</table>