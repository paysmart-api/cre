---
title: Translate
name: translate
tags:
    - pps
    - translate
    - endpoint
description: Endpoint para pedidos de tradução de dados.
type: pps
method: POST
---
{% include endpoint_header.html %}

### **Argumentos para tradução de PIN Blocks cifrados**  

<table>
    <thead>
        <tr>
            <th>Nome</th>
            <th>Tipo</th>
            <th>Descrição</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>PAN</td>
            <td>String</td>
            <td>Valor entre 16 e 19 dígitos.</td>
        </tr>
        <tr>
            <td>PINBlock</td>
            <td>String</td>
            <td>PIN Block cifrado que deve ser traduzido. Valor em Hexadecimal.</td>
        </tr>
    </tbody>
</table>

Os comandos a seguir necessitam de dois argumentos principais: **InputEnc** e **OutputEnc**, representados por objetos.<br>
Dentro destes objetos, é necessário especificar outros argumentos, obrigatórios para a tradução dos dados. Segue abaixo a tabela com a explicação dos mesmos.

<table>
    <thead>
        <tr>
            <th>Nome</th>
            <th>Tipo</th>
            <th>Descrição</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>KeyIdentifier</td>
            <td>String</td>
            <td>Valor de 4 dígitos hexadecimais usado para identificar qual chave criptográfica está sendo usada.</td>
        </tr>
        <tr>
            <td>Format</td>
            <td>String</td>
            <td>Indica o formato do PIN Block. 00 para ISO-0 e 02 para ISO-2. É altamente recomendado que o formato ISO-0 seja utilizado. O suporte para ISO-2 é considerado deprecated.</td>
        </tr>
    </tbody>
</table>

### **Argumentos para outros tipos de tradução**

Todos comandos necessitam de dois argumentos principais: **InputEnc** e **OutputEnc**, representados por objetos.<br>
Dentro destes objetos, é necessário especificar outros argumentos, obrigatórios para a tradução dos dados. Segue abaixo a tabela com a explicação dos mesmos.   

<table>
    <thead>
        <tr>
            <th>Nome</th>
            <th>Tipo</th>
            <th>Descrição</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Method</td>
            <td>String</td>
            <td>Método para tradução (WK ou DUKPT).</td>
        </tr>
        <tr>
            <td>KeyIdentifier</td>
            <td>String</td>
            <td>Valor de 4 dígitos hexadecimais usado para identificar qual chave de transporte está sendo usada. <b>Obrigatório caso o método seja WK.</b></td>
        </tr>
        <tr>
            <td>WorkingKey</td>
            <td>String</td>
            <td>Chave usada para cifrar os dados. Essa chave deve ser transmitida no comando criptografada pela chave mestre identificada pelo KeyIdentifier. <b>Obrigatório caso o método seja WK.</b></td>
        </tr>
        <tr>
            <td>BDKIdentifier</td>
            <td>String</td>
            <td>Identificador de 4 dígitos hexadecimais que indica qual BDK deve ser usada. <b>Obrigatório caso o método seja DUKPT.</b></td>
        </tr>
        <tr>
            <td>KSN</td>
            <td>String</td>
            <td>KSN retornado pelo equipamento que realizou a criptografia dos dados usando a chave DUKPT. <b>Obrigatório caso o método seja DUKPT.</b></td>
        </tr>
        <tr>
            <td>DataType</td>
            <td>String</td>
            <td>Tipo de dados. Pode ser 0 (PIN) ou 1 (Dado genérico). <b>Obrigatório caso o método seja DUKPT.</b></td>
        </tr>
        <tr>
            <td>Mode</td>
            <td>String</td>
            <td>Modo de cifragem dos dados. Pode ser 0 (ECB) ou 1 (CBC). Se não for informado o valor, então 0 (ECB) é usado por padrão. <b>Caso o método de entrada seja DUKPT e saída WK, então utiliza-se obrigatoriamente o valor 0 (ECB).</b></td>
        </tr>
        <tr>
            <td>Data</td>
            <td>String</td>
            <td>Dados criptografados pelo mecanismo escolhido (WK ou DUKPT).</td>
        </tr>
        <tr>
            <td>ConvertToChar</td>
            <td>String</td>
            <td>Se os dados devem ser convertidos para char (1) ou não (0) antes da cifragem. Se não for informado o valor 0 (não) é usado por padrão. <b>Presente somente no OutputEnc e obrigatório caso o método seja WK.</b></td>
        </tr>
    </tbody>
</table>

Além dos argumentos InputEnc e OutputEnc, pode-se adicionar o argumento opcional **EchoField**, que será um string ecoado na resposta caso venha preenchido. Valor com 1 a 256 caracteres ASCII.

### **Exemplos de Requisição**
Atualmente, o serviço CRE suporta três métodos de tradução de chaves:

* Tradução de dados de uma Working Key para outra Working Key.
```
curl --key client.key --cert client.crt \
-X POST https://api.paysmart.com.br:60443/api/v1/pps/translate \
-H "Content-type: application/json" \
-H "x-api-key: 1e42fc40-9532-4269-9a03-7be0949ef0c7" \
{
    "InputEnc": {
        "Method": "WK",
        "KeyIdentifier": "0001", 
        "WorkingKey": "CA7C368C52D4C4041ED47FECBE53004B", 
        "Data": "61B3D2D2BF821434", 
        "Mode": "0"
    },
    "OutputEnc": {
        "Method": "WK",
        "KeyIdentifier": "1000",
        "WorkingKey": "0B40550034890483A522D4E6BEC60685",
        "Mode": "0",
        "ConvertToChar": "0"
    }
}
```

* Tradução de dados de uma chave DUKPT para uma Working Key.
```
curl -X POST https://api.paysmart.com.br:60443/api/v1/pps/translate \
-H "Content-type: application/json" \
-H "x-api-key: 1e42fc40-9532-4269-9a03-7be0949ef0c7" \
{
    "InputEnc": {
        "Method": "DUKPT",
        "BDKIdentifier": "0001", 
        "KSN": "FFFF9876543210E10008", 
        "DataType": "0",
        "Data": "61B3D2D2BF821434", 
        "Mode": "0" 
    },
    "OutputEnc": {
        "Method": "WK",
        "KeyIdentifier": "1000",
        "WorkingKey": "0B40550034890483A522D4E6BEC60685",
        "Mode": "0",
        "ConvertToChar": "0"
    }
}
```

* Tradução de dados de uma chave DUKPT para outra chave DUKPT.
```
curl -X POST https://api.paysmart.com.br:60443/api/v1/pps/translate \
-H "Content-type: application/json" \
-H "x-api-key: 1e42fc40-9532-4269-9a03-7be0949ef0c7" \
{
    "InputEnc": {
        "Method": "DUKPT",
        "BDKIdentifier": "0001", 
        "KSN": "FFFF9876543210E10008", 
        "DataType": "0",
        "Data": "61B3D2D2BF821434", 
        "Mode": "1"
    },
    "OutputEnc": {
        "Method": "DUKPT",
        "BDKIdentifier": "0002",
        "KSN": "FFFF000543210E10003",
        "Mode": "1"
    },
    "EchoField": "Teste"
}
```

* Tradução de PIN Blocks cifrados.
```
curl --key client.key --cert client.crt \
-X POST https://api.paysmart.com.br:60443/api/v1/pps/translate \
-H "Content-type: application/json" \
-H "x-api-key: 1e42fc40-9532-4269-9a03-7be0949ef0c7" -d '
{
    "PAN": "1234567890123456",
    "PINBlock": "195917993CA1F656",
    "InputEnc": {
        "KeyIdentifier": "FFFF",
        "Format": "00"
    },
    "OutputEnc": {
        "KeyIdentifier": "FFFF",
        "Format": "00"
    }
}'
```

### **Exemplo de Resposta**

```json
{
    "resultCode": 0,
    "resultDescription": "Comando processado com sucesso",
    "psResponseId": "dfd88b95-d28d-49ac-90ba-7ba0b35aebd0",
    "dataTranslated": "696D7E00DF6201B0",
    "echoField": "Teste"
}
```

### **Descrição da Resposta**

<table>
    <thead>
        <tr>
            <th>Nome</th>
            <th>Tipo</th>
            <th>Descrição</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>resultCode</td>
            <td>Integer</td>
            <td>Código de resultado do processamento.</td>
        </tr>
        <tr>
            <td>resultDescription</td>
            <td>String</td>
            <td>Descrição textual do resultado do processamento.</td>
        </tr>
        <tr>
            <td>psResponseID</td>
            <td>String</td>
            <td>Identificador único da resposta. Gerado pela paySmart.</td>
        </tr>
        <tr>
            <td>dataTranslated</td>
            <td>String</td>
            <td>Dados traduzidos.</td>
        </tr>
        <tr>
            <td>echoField <b>[Opcional]</b></td>
            <td>String</td>
            <td>Campo com o valor informado na requisição.</td>
        </tr>
    </tbody>
</table>

{% include endpoint_footer.html %}